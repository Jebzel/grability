//
//  MCConstants.swift
//  Movie Catalog
//
//  Created by Jebzel Castillo on 6/27/17.
//  Copyright © 2017 Jebzel Castillo. All rights reserved.
//

import Foundation

struct MCConstants {
 

   static let apiKey = "426437b1c96988f15ec2fef6606c314a"
   //static let session_id = "035c9a789c9714a43c7237cde2b93d03ad81bf85"

    struct Region {
        
        static let  language = "es-ES"
        static let region = "CO"
        
    }
    struct TV {
        
        static let sort_by = "created_at.asc"
        static let session_id = "1"
        
    }

    struct URL {
        fileprivate static let DEVELOPMENT_URL = "https://api.themoviedb.org/3"
        fileprivate static let PRODUCTION_URL = "https://api.themoviedb.org/3"
        fileprivate static let IMAGE_URL = "https://image.tmdb.org/t/p/w500"
 
        static var BASE_URL: String {
            return DEVELOPMENT_URL
        }
        static var Session: String! {
            return BASE_URL + "/authentication/guest_session/new"
        }
 
        static var Search_Movie: String! {
            return BASE_URL + "/search/movie"
        }
        static var Movie_Popular: String! {
            return BASE_URL + "/movie/popular"
        }
        static var Movie_Rated: String! {
            return BASE_URL + "/movie/top_rated"
        }
        static var Movie_Upcoming: String! {
            return BASE_URL + "/movie/upcoming"
        }
        
        static var Search_Show: String! {
            return BASE_URL + "/search/tv"
        }
        
        static var Show_Popular: String! {
            return BASE_URL + "/tv/popular"
        }
        static var Show_Rated: String! {
            return BASE_URL + "/tv/top_rated"
        }
        static var Show_Latest: String! {
            return BASE_URL + "/tv/latest"
        }
        
        static var Image_Base: String! {
            return IMAGE_URL
        }
    }
}
