//
//  MCMovie.swift
//  Movie Catalog
//
//  Created by Jebzel Castillo on 6/28/17.
//  Copyright © 2017 Jebzel Castillo. All rights reserved.
//

import Foundation
import SwiftyJSON

class MCMovie {
    
    fileprivate static let VOTE_COUNT = "vote_count"
    fileprivate static let ID = "id"
    fileprivate static let VIDEO = "video"
    fileprivate static let VOTE_AVERAGE = "vote_average"
    fileprivate static let TITLE = "title"
    fileprivate static let POPULARITY = "popularity"
    fileprivate static let POSTER_PATH = "poster_path"
    fileprivate static let ORIGINAL_LANGUAGE = "original_language"
    fileprivate static let ORIGINAL_TITLE = "original_title"
    fileprivate static let GENRE_IDS = "genre_ids"
    fileprivate static let BACKDROP_PATH = "backdrop_path"
    fileprivate static let ADULT = "adult"
    fileprivate static let OVERVIEW = "overview"
    fileprivate static let RELEASE_DATE = "release_date"
    
    fileprivate var _vote_count : Int!
    fileprivate var _id : Int!
    fileprivate var _video : Bool!
    fileprivate var _vote_average : Double!
    fileprivate var _title : String!
    fileprivate var _popularity : Double!
    fileprivate var _poster_path : String!
    fileprivate var _original_language : String!
    fileprivate var _original_title : String!
    fileprivate var _genre_ids : [Int]!
    fileprivate var _backdrop_path : String!
    fileprivate var _adult : Bool!
    fileprivate var _overview : String!
    fileprivate var _release_date : String!


    var vote_count : Int {
        get { return _vote_count ?? 0 }
    }
    
    var id : Int {
        get { return _id ?? 0 }
    }
    
    var video : Bool {
        get { return _video ?? false }
    }
    
    var vote_average : Double {
        get { return _vote_average ?? 0.0 }
    }
    
    var title : String {
        get { return _title ?? "" }
    }
    
    var popularity : Double {
        get { return _popularity ?? 0.0 }
    }
    
    var poster_path : String {
        get { return _poster_path ?? "" }
    }
    
    var original_language : String {
        get { return _original_language ?? "Información no disponible" }
    }
    
    var original_title : String {
        get { return _original_title ?? "Título no disponible" }
    }
    
    var genre_ids : [Int] {
        get { return _genre_ids ?? [Int]() }
    }
    
    var backdrop_path : String {
        get { return _backdrop_path ?? "" }
    }
    
    var adult : Bool {
        get { return _adult ?? false }
    }
    
    var overview : String {
        get { return _overview ?? "Información no disponible" }
    }
    
    var release_date : String {
        get { return _release_date ?? "Información no disponible"}
    }

    init(data: JSON) {
        _vote_count = data[MCMovie.VOTE_COUNT].int
        _id = data[MCMovie.ID].int
        _video = data[MCMovie.VIDEO].bool
        _vote_average = data[MCMovie.VOTE_AVERAGE].double
        _title = data[MCMovie.TITLE].string
        _popularity = data[MCMovie.POPULARITY].double
        _poster_path = data[MCMovie.POSTER_PATH].string
        _original_language = data[MCMovie.ORIGINAL_LANGUAGE].string
        _original_title = data[MCMovie.ORIGINAL_TITLE].string
 
       _genre_ids =  data[MCMovie.GENRE_IDS].arrayValue.map { $0.int! }
        _backdrop_path = data[MCMovie.BACKDROP_PATH].string
        _adult = data[MCMovie.ADULT].bool
        _overview = data[MCMovie.OVERVIEW].string
        _release_date = data[MCMovie.RELEASE_DATE].string
        
      
    }//search/movie?
    static func searchMovie(page: Int, query: String, _ done: @escaping (_ movies: NSMutableArray?, _ msg: String, _ currentPage: Int?, _ totalPage: Int? ) -> Void) {
        let parameters: [String: Any] = [
            "api_key" : MCConstants.apiKey,
            "language" : MCConstants.Region.language,
            "page" : page,
            "query" :  query,
             "region" : "US",
            ]
        
        MCNetwork.makeRequest(MCConstants.URL.Search_Movie, params: parameters) { (response, msg) in
            guard let movies = response?["results"].array
                
                else {
                    return done(nil, msg, nil, nil)
            }
            
            let newsArray = NSMutableArray()
            let page = response?["page"].int
            let totalPages = response?["total_pages"].int
            for movie in movies {
                newsArray.add(MCMovie(data: movie))
            }
            
            done(newsArray, "", page, totalPages)
        }
    }
    static func getPopular(page: Int, _ done: @escaping (_ movies: NSMutableArray?, _ msg: String, _ currentPage: Int?, _ totalPage: Int? ) -> Void) {
        let parameters: [String: Any] = [
            "api_key" : MCConstants.apiKey,
            "language" : MCConstants.Region.language,
            "page" : page,
            "region" :  MCConstants.Region.region,
        ]

        MCNetwork.makeRequest(MCConstants.URL.Movie_Popular, params: parameters) { (response, msg) in
            guard let movies = response?["results"].array

                else {
                     return done(nil, msg, nil, nil)
            }
            
            let newsArray = NSMutableArray()
            let page = response?["page"].int
            let totalPages = response?["total_pages"].int
            for movie in movies {
                newsArray.add(MCMovie(data: movie))
             }
            
            done(newsArray, "", page, totalPages)
        }
    }
    static func getRated(page: Int, _ done: @escaping (_ movies: NSMutableArray?, _ msg: String, _ currentPage: Int?, _ totalPage: Int? ) -> Void) {
        let parameters: [String: Any] = [
            "api_key" : MCConstants.apiKey,
            "language" : MCConstants.Region.language,
            "page" : page,
            "region" :  MCConstants.Region.region,
            ]
        
        MCNetwork.makeRequest(MCConstants.URL.Movie_Rated, params: parameters) { (response, msg) in
            guard let movies = response?["results"].array
                
                else {
                     return done(nil, msg, nil, nil)
            }
            
           let newsArray = NSMutableArray()
            let page = response?["page"].int
            let totalPages = response?["total_pages"].int
            for movie in movies {
                newsArray.add(MCMovie(data: movie))
            }
            
            done(newsArray, "", page, totalPages)
        }
    }
    static func getUpcoming(page: Int, _ done: @escaping (_ movies: NSMutableArray?, _ msg: String, _ currentPage: Int?, _ totalPage: Int? ) -> Void) {
        let parameters: [String: Any] = [
            "api_key" : MCConstants.apiKey,
            "language" : MCConstants.Region.language,
            "page" : page,
           // "region" :  MCConstants.Region.region,
            ]
        
        MCNetwork.makeRequest(MCConstants.URL.Movie_Upcoming, params: parameters) { (response, msg) in
            guard let movies = response?["results"].array
                
                else {
                   return done(nil, msg, nil, nil)
            }
            
            let newsArray = NSMutableArray()
            let page = response?["page"].int
            let totalPages = response?["total_pages"].int
            for movie in movies {
                newsArray.add(MCMovie(data: movie))
            }
            
            done(newsArray, "", page, totalPages)
        }
    }

}
