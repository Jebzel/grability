//
//  MCShow.swift
//  Movie Catalog
//
//  Created by Jebzel Castillo on 7/3/17.
//  Copyright © 2017 Jebzel Castillo. All rights reserved.
//


import Foundation
import SwiftyJSON

class MCShow {
    fileprivate static let POSTER_PATH = "poster_path"
    fileprivate static let POPULARITY = "popularity"
    fileprivate static let ID = "id"
    fileprivate static let BACKDROP_PATH = "backdrop_path"
    fileprivate static let VOTE_AVERAGE = "vote_average"
    fileprivate static let OVERVIEW = "overview"
    fileprivate static let FIRST_AIR_DATE = "first_air_date"
    fileprivate static let ORIGIN_COUNTRY = "origin_country"
    fileprivate static let GENRE_IDS = "genre_ids"
    fileprivate static let ORIGINAL_LANGUAGE = "original_language"
    fileprivate static let VOTE_COUNT = "vote_count"
    fileprivate static let NAME = "name"
    fileprivate static let ORIGINAL_NAME = "original_name"

    fileprivate var _poster_path : String!
    fileprivate var _popularity : Double!
    fileprivate var _id : Int!
    fileprivate var _backdrop_path : String!
    fileprivate var _vote_average : Double!
    fileprivate var _overview : String!
    fileprivate var _first_air_date : String!
    fileprivate var _origin_country : [String]!
    fileprivate var _genre_ids : [Int]!
    fileprivate var _original_language : String!
    fileprivate var _vote_count : Int!
    fileprivate var _name : String!
    fileprivate var _original_name : String!
 
    var poster_path : String {
        get { return _poster_path ?? "" }
    }
    
    var popularity : Double {
        get { return _popularity ?? 0.0 }
    }
    
    var id : Int {
        get { return _id ?? 0 }
    }
    
    var backdrop_path : String {
        get { return _backdrop_path ?? "" }
    }
    
    var vote_average : Double {
        get { return _vote_average ?? 0.0 }
    }
    
    var overview : String {
        get { return _overview ?? "Información no disponible" }
    }
    
    var first_air_date : String {
        get { return _first_air_date ?? "Información no disponible" }
    }
    
    var origin_country : [String] {
        get { return _origin_country ?? [String]() }
    }
    
    var genre_ids : [Int] {
        get { return _genre_ids ?? [Int]() }
    }
    
    var original_language : String {
        get { return _original_language ?? "Información no disponible" }
    }
    
    var vote_count: Int {
        get { return _vote_count ?? 0 }
    }
    
    var name : String {
        get { return _name ?? "Nombre no disponible" }
    }
    
    var original_name : String {
        get { return _original_name ?? "Título no disponible" }
    }
    
    init(data: JSON) {
        _poster_path = data[MCShow.POSTER_PATH].string
        _popularity = data[MCShow.POPULARITY].double
        _id = data[MCShow.ID].int
        _backdrop_path = data[MCShow.BACKDROP_PATH].string
        _vote_average = data[MCShow.VOTE_AVERAGE].double
        _overview = data[MCShow.OVERVIEW].string
        _first_air_date = data[MCShow.FIRST_AIR_DATE].string
        _origin_country = data[MCShow.ORIGIN_COUNTRY].arrayValue.map { $0.string! }
        _genre_ids =  data[MCShow.GENRE_IDS].arrayValue.map { $0.int! }
        _original_language = data[MCShow.ORIGINAL_LANGUAGE].string
        _vote_count = data[MCShow.VOTE_COUNT].int
        _name = data[MCShow.NAME].string
        _original_name = data[MCShow.ORIGINAL_NAME].string
        
    }
    static func searchShow (page: Int, query: String, _ done: @escaping (_ shows: NSMutableArray?, _ msg: String, _ currentPage: Int?, _ totalPage: Int? ) -> Void) {
        let parameters: [String: Any] = [
            "api_key" : MCConstants.apiKey,
            "language" : MCConstants.Region.language,
            "page" : page,
            "query" :  query,
            "region" : "US",
            ]
        
        MCNetwork.makeRequest(MCConstants.URL.Search_Show, params: parameters) { (response, msg) in
            guard let shows = response?["results"].array
                
                else {
                    return done(nil, msg, nil, nil)
            }
            
            let newsArray = NSMutableArray()
            let page = response?["page"].int
            let totalPages = response?["total_pages"].int
            for show in shows {
                newsArray.add(MCShow(data: show))
            }
            
            done(newsArray, "", page, totalPages)
        }
    }
    
    static func getPopular(page: Int, _ done: @escaping (_ shows: NSMutableArray?, _ msg: String,_ currentPage: Int?, _ totalPage: Int? ) -> Void) {
        let parameters: [String: Any] = [
            "api_key" : MCConstants.apiKey,
            "language" : MCConstants.Region.language,
            "page" : page,
            ]
        
        MCNetwork.makeRequest(MCConstants.URL.Show_Popular, params: parameters) { (response, msg) in
            guard let shows = response?["results"].array
            else {
                return done(nil, msg, nil, nil)
            }
            
            let newsArray = NSMutableArray()
            let page = response?["page"].int
            let totalPages = response?["total_pages"].int
            for show in shows {
                newsArray.add(MCShow(data: show))
            }
            done(newsArray, "", page, totalPages)
        }
    }
    static func getRated(page: Int, _ done: @escaping (_ shows: NSMutableArray?, _ msg: String, _ currentPage: Int?, _ totalPage: Int? ) -> Void) {
        let parameters: [String: Any] = [
            "api_key" : MCConstants.apiKey,
            "language" : MCConstants.Region.language,
            "page" : page,
            ]
        
        MCNetwork.makeRequest(MCConstants.URL.Show_Rated, params: parameters) { (response, msg) in
            guard let shows = response?["results"].array
             else {
                return done(nil, msg, nil, nil)
            }
            
            let newsArray = NSMutableArray()
            let page = response?["page"].int
            let totalPages = response?["total_pages"].int
            for show in shows {
                newsArray.add(MCShow(data: show))
            }
            
            done(newsArray, "", page, totalPages)
        }
    }
    static func getLatest(page: Int, _ done: @escaping (_ shows: NSMutableArray?, _ msg: String, _ currentPage: Int?, _ totalPage: Int? ) -> Void) {
        let parameters: [String: Any] = [
            "api_key" : MCConstants.apiKey,
            "language" : MCConstants.Region.language,
            "page" : page,
            ]
        
        MCNetwork.makeRequest(MCConstants.URL.Show_Latest, params: parameters) { (response, msg) in
            guard let shows = response?["results"].array
                else {
                    return done(nil, msg, nil, nil)
            }
            
            let newsArray = NSMutableArray()
            let page = response?["page"].int
            let totalPages = response?["total_pages"].int
            for show in shows {
                newsArray.add(MCShow(data: show))
            }
            
            done(newsArray, "", page, totalPages)
        }
    }


}
