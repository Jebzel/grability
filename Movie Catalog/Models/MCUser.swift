//
//  MCUser.swift
//  Movie Catalog
//
//  Created by Jebzel Castillo on 7/19/17.
//  Copyright © 2017 Jebzel Castillo. All rights reserved.
//

import Foundation
import SwiftyJSON

class MCUser {
    
    fileprivate static let GUEST_SESSION_ID = "guest_session_id"
    
    fileprivate var _guest_session_id : String!
    
    var guest_session_id : String {
        get { return (_guest_session_id ?? nil)! }
    }
    
    init(session: String) {
        _guest_session_id = session
        
    }

    static func getSession(_ done: @escaping (_ session: String?, _ message: String) -> Void) {
        let parameters: [String: String] = [
            "api_key" : MCConstants.apiKey,
        ]
         MCNetwork.makeRequest(MCConstants.URL.Session, params: parameters) { (response, msg) in
            
            guard let session = response?.dictionary![MCUser.GUEST_SESSION_ID]?.string
                
            else {
                    return done(nil, msg)
            }

            done(session, msg)
        }
    }
}
