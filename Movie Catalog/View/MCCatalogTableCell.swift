//
//  MCCatalogTableCell.swift
//  Movie Catalog
//
//  Created by Jebzel Castillo on 6/30/17.
//  Copyright © 2017 Jebzel Castillo. All rights reserved.
//

import UIKit

class CatalogTableCell: UITableViewCell {
    
        
    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var dobLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
        
}
