//
//  MCNetwork.swift
//  Movie Catalog
//
//  Created by Jebzel Castillo on 6/27/17.
//  Copyright © 2017 Jebzel Castillo. All rights reserved.
//

import Foundation
import SystemConfiguration
import Alamofire
import SwiftyJSON


class MCNetwork {
    
    fileprivate static var errorMsg = "Hubo un error"
    
    
    static func makeRequest(_ url: String, params: [String:Any], done: @escaping (_ response: JSON?, _ msg: String) -> Void) {
        
        if isConnectedToNetwork() {
            Alamofire.request(url, parameters: params).responseJSON { response in

                if response.result.isSuccess {
                    
                    guard let value = response.result.value
                        else {
                            return done(nil, errorMsg)
                    }
                    
                   let json = JSON(value)
                   if json.error  != nil
                   {
                        done(nil, json["status_message"].string ?? errorMsg)
                    
                    }
                   else {
                         done(json, "")
                    }
                    
                } else {
                    print("Error")
                    done(nil, errorMsg)
                }
            }
        } else {
            done(nil, "Necesita tener conexión a Internet")
        }
    }
}

extension MCNetwork {
    
    static func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        return (isReachable && !needsConnection)
    }
}
