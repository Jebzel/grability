//
//  ViewController.swift
//  Movie Catalog
//
//  Created by Jebzel Castillo on 6/27/17.
//  Copyright © 2017 Jebzel Castillo. All rights reserved.
//

import UIKit
import SDWebImage

class MCMainView: UIViewController {

    @IBOutlet var titles : [UILabel]!
    @IBOutlet var images : [UIImageView]!
    @IBOutlet var buttons : [UIButton]!

    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var movies = NSMutableArray()
    var shows = NSMutableArray()
    var session = String()
    var btnOption = Int()
    var page : Int = 1
    var totalPages = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .random()
        
         MCUser.getSession { (session, msg) in
            if session != nil {
                self.session = session!
                print ("Sesión para usuario invitado Obtenida \(session!)")
            } else {
                 print ("Error obteniendo sesión para usuario invitado \(msg)")
            }
        }
        MCMovie.getPopular(page: 1) { (movies, msg, currentPage, totalPage) in
            if movies != nil {
                self.movies = movies!
                self.totalPages = totalPage!
                DispatchQueue.main.async(execute: { () -> Void in
                    self.setTitlesAndImages(movie: movies, show: nil)
                })
            } else {
                print ("sin peliculas \(msg)")
            }
        }
       
     }

    
    func setTitleAndImage<T>(outlets: [T], movie: NSMutableArray?, show: NSMutableArray?) {

        var urlString = String()
        var titleArray: [String] =  ["Popular", "Top Rated", "Upcoming"]
        for index in 0..<outlets.count {
            if movie != nil {
                 urlString = MCConstants.URL.Image_Base + (movies[index] as! MCMovie).backdrop_path
            } else {
                 urlString = MCConstants.URL.Image_Base + (shows[index]as! MCShow).backdrop_path
            }
            
            if let label = outlets[index] as? UILabel {
                label.text = titleArray[index]
            } else if let view = outlets[index] as? UIImageView {
                let url = URL(string: urlString)
                view.sd_setImage(with: url as URL!, placeholderImage: UIImage(named: "placeholder"), options: .refreshCached)
            } else if let button = outlets[index] as? UIButton {
                button.backgroundColor = .random()
            }
        }
    }
    func setTitlesAndImages(movie: NSMutableArray?, show: NSMutableArray?) {
        self.setTitleAndImage(outlets: self.titles, movie: movie, show: show)
        self.setTitleAndImage(outlets: self.images, movie: movie, show: show)
        self.setTitleAndImage(outlets: self.buttons, movie: movie, show: show)

        self.view.backgroundColor = UIColor.white
    }

    
    @IBAction func indexChanged(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            self.setTitlesAndImages(movie: movies, show: nil)
           // print("First Segment Selected")
        case 1:
            if self.shows.count == 0 {
                MCShow.getPopular(page: 1) { (shows, msg, currentPage, totalPage) in
                    if shows != nil {
                        self.shows = shows!
                        self.totalPages = totalPage!
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.setTitlesAndImages(movie: nil, show: shows)
                        })
                    } else {
                        print ("sin series \(msg)")
                    }
                }

            } else {
                self.setTitlesAndImages(movie: nil, show: shows)
            }
           // print("Second Segment Selected")
        default:
            break
        }
    }
    
    @IBAction func btnOption1(_ sender: Any) {
         btnOption = 1
    }
    
    @IBAction func btnOption2(_ sender: Any) {
        btnOption = 2
     }
    
    
    @IBAction func btnOption3(_ sender: Any) {
        btnOption = 3
    }

    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MCCatalogView {
            vc.segmentedControl =  segmentedControl.selectedSegmentIndex
            vc.option = btnOption
            vc.page = page
            if btnOption == 1 {
                if segmentedControl.selectedSegmentIndex == 0 {
                    vc.elements = movies
                } else {
                    vc.elements = shows
                }
                
            }
        }
    }

}

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}

extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 0.5)
    }
}
