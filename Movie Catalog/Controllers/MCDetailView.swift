//
//  MCDetailView.swift
//  Movie Catalog
//
//  Created by Jebzel Castillo on 7/18/17.
//  Copyright © 2017 Jebzel Castillo. All rights reserved.
//


import UIKit

class MCDetailView: UIViewController {
    
    
    @IBOutlet weak var posterImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var voteCount: UILabel!
    @IBOutlet weak var relaseDateLabel: UILabel!
    
    
    var detailMovie: MCMovie?
    var detailShow: MCShow?
    
    func configureView() {
        // Update the user interface for the detail item.
        var title =  String()
        var urlString = String()
        var overView = String()
        var releaseDate = String()
        var popularity = Int()

        if let detail = self.detailMovie {
            title =  detail.original_title
            urlString = MCConstants.URL.Image_Base + detail.poster_path
            popularity = detail.vote_count
            overView = detail.overview
            releaseDate = detail.release_date
        } else {
            title =   self.detailShow!.original_name
            urlString = MCConstants.URL.Image_Base + self.detailShow!.poster_path
            popularity = self.detailShow!.vote_count
            overView = self.detailShow!.overview
            releaseDate = self.detailShow!.first_air_date
        }
        if (releaseDate == "") {
            self.relaseDateLabel.text = "Estrenada el \(self.getFormatDate(releaseDate))"
        }else {
            self.relaseDateLabel.text = "Sin fecha de estreno"
        }
        
        
        self.titleLabel.text = title
        self.voteCount.text = "Votos : \(popularity)"
        self.overviewLabel.text = overView
    
        let url = URL(string: urlString)
        self.posterImage.sd_setImage(with: url as URL!, placeholderImage: UIImage(named: "placeholder"), options: .refreshCached)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureView()
    }
    
    
    //MARK : - Utils
    
    func getFormatDate(_ dateString: String?) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
       
        
        let dateObj = dateFormatter.date(from: dateString!)
        
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return (dateFormatter.string(from: dateObj!))

    }
    
    
}

