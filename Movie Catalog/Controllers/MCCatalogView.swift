//
//  MCCatalogView.swift
//  Movie Catalog
//
//  Created by Jebzel Castillo on 6/29/17.
//  Copyright © 2017 Jebzel Castillo. All rights reserved.
//


import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class MCCatalogView: UIViewController,UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate{
    
   
   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var footerView: UIView!
    
     var segmentedControl = Int()
     var option = Int()
     var page = Int()

     var isLoading : Bool = false
     var elements = NSMutableArray()
    var filteredElements = NSMutableArray()
     var searchActive : Bool = false
     var mySearchText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if option > 1 {
            self.loadData() { (elements) in
                if elements != nil {
                    self.elements = elements!
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
        } 
    }
 
    @IBAction func searchButton(_ sender: Any) {
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return elements.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var title =  String()
        var urlString = String()
        var popularity = Double()
        
        if segmentedControl == 0 {
            title = (elements[indexPath.row] as! MCMovie).original_title
            if (elements[indexPath.row] as! MCMovie).backdrop_path == "" {
                urlString = MCConstants.URL.Image_Base + (elements[indexPath.row] as! MCMovie).poster_path
            } else {
                urlString = MCConstants.URL.Image_Base + (elements[indexPath.row] as! MCMovie).backdrop_path
            }
            popularity = (elements[indexPath.row] as! MCMovie).vote_average
        } else {
            title = (elements[indexPath.row] as! MCShow).original_name
            if (elements[indexPath.row] as! MCShow).backdrop_path == "" {
                urlString = MCConstants.URL.Image_Base + (elements[indexPath.row] as! MCShow).poster_path
            } else {
                urlString = MCConstants.URL.Image_Base + (elements[indexPath.row] as! MCShow).backdrop_path
            }
            popularity = (elements[indexPath.row] as! MCShow).vote_average
        }
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! CatalogTableCell
       // let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)  as! CatalogTableCell
        cell.tag = indexPath.row
        //print ("tag: \(cell.tag)")
        cell.nameLabel.text = title
        cell.dobLabel.text =  String (popularity)
        let url = URL(string: urlString)
       
        cell.imgView.sd_setImage(with: url as URL!, placeholderImage: UIImage(named: "movie-placeholder.jpg"), options: .refreshCached)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 144
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset.y
        let maxOffset = scrollView.contentSize.height - scrollView.frame.size.height
        if (maxOffset - offset) <= 0 {
            if (!self.isLoading) {
                self.isLoading = true
                loadCell(elements.count)
            }
        }
    }
    //search
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(searchText == ""){
            searchActive = false
        }else{
            searchActive = true
        }
        //print ("Pelicula \(searchText)")
        
        
        if(self.searchActive){
            if self.segmentedControl == 0 {
                MCMovie.searchMovie(page: 1, query: searchText) { (movie, msg, currentPage, totalPage) in
                    if movie != nil {
                        self.elements = movie!
                        self.reloadSearch(searchText: searchText)
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.tableView.reloadData()
                        })
                    }
                }
            } else {
                MCShow.searchShow(page: 1, query: searchText) { (show, msg, currentPage, totalPage) in
                    if show != nil {
                        self.elements = show!
                        self.reloadSearch(searchText: searchText)
                        DispatchQueue.main.async(execute: { () -> Void in
                            self.tableView.reloadData()
                        })
                    }
                }
            }
        } else{
            self.loadData() { (elements) in
                if elements != nil {
                    self.elements = elements!
                    self.reloadSearch(searchText: searchText)
                    DispatchQueue.main.async(execute: { () -> Void in
                        self.tableView.reloadData()
                    })
                }
            }
        }
        
     }
    
    private func reloadSearch(searchText: String)
    {
      page = 1
       mySearchText = searchText
        
    }
    
   
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(searchActive){
//            print ("index path \(indexPath.row+1 ) elementos: \(elements.count ) ")
//            if indexPath.row+1 == elements.count {
//                let topPath = IndexPath(row: 0, section: 0)
//                self.tableView.scrollToRow(at: topPath, at: UITableViewScrollPosition.top, animated: false)
//            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

    }
   
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchActive = false
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.loadData() { (elements) in
            if elements != nil {
                self.elements = elements!
                DispatchQueue.main.async(execute: { () -> Void in
                    self.tableView.reloadData()
                })
            }
        }

        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func loadCell(_ offset : Int){
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            // this runs on the background queue
            // here the query starts to add new 20 rows of data to arrays
            if offset != 0 {
                sleep(2)
            }
            
                self.page += 1
                if(self.searchActive){
                    if self.segmentedControl == 0 {
                        MCMovie.searchMovie(page: self.page, query: self.mySearchText) { (movie, msg, currentPage, totalPage) in
                            if movie != nil {
                                 self.loadNewElements(newElements: movie!)
                                                            }
                        }
                    } else {
                        MCShow.searchShow(page: self.page, query: self.mySearchText) { (show, msg, currentPage, totalPage) in
                            if show != nil {
                                self.loadNewElements(newElements: show!)
                               
                            }
                        }
                    }
                }else{
                    self.loadData() { (elements) in
                        if elements != nil {
                           self.loadNewElements(newElements: elements!)
                            
                        }
                    }

                }
        }
        
    }
    
    func loadNewElements (newElements: NSMutableArray) {
        for element in newElements {
            let row = self.elements.count
            let indexPath = IndexPath(row:row,section:0)
            self.elements.add(element)
           self.tableView?.insertRows(at: [indexPath], with: UITableViewRowAnimation.fade)
        }
         //self.tableView.reloadData()
         self.isLoading = false

    }
    
    func loadData(_ done: @escaping (_ elements: NSMutableArray?) -> Void) {
   
        switch segmentedControl
        {
        case 0:
            switch option
            {
            case 1:
                MCMovie.getPopular(page: page) { (movie, msg, currentPage, totalPage) in
                    if movie != nil {
                        done (movie!)
                    } else {
                         done (nil)
                        print ("sin peliculas \(msg)")
                    }
                }
            case 2:
                MCMovie.getRated(page: page) { (movie, msg, currentPage, totalPage) in
                    if movie != nil {
                        done (movie!)
                    } else {
                        done (nil)
                        print ("sin peliculas \(msg)")
                    }
                }
            case 3:
                MCMovie.getUpcoming(page: page) { (movie, msg, currentPage, totalPage) in
                    if movie != nil {
                        done (movie!)
                    } else {
                        done (nil)
                        print ("sin peliculas \(msg)")
                    }
                }
            default:
                break
            }
            
        case 1:
            switch option
            {
            case 1:
                MCShow.getPopular(page: page) { (shows, msg, currentPage, totalPage) in
                    if shows != nil {
                        done (shows!)
                    } else {
                        done (nil)
                        print ("sin series \(msg)")
                    }
                }
            case 2:
                MCShow.getRated(page: page) { (shows, msg, currentPage, totalPage) in
                    if shows != nil {
                      done (shows!)
                        
                    } else {
                        done (nil)
                        print ("sin series \(msg)")
                    }
                }
            case 3:
                MCShow.getLatest(page: page) { (shows, msg, currentPage, totalPage) in
                    if shows != nil {
                       done (shows!)
                    } else {
                        done (nil)
                        print ("sin series \(msg)")
                    }
                }
            default:
                break
            }
        default:
            break
        }
        
        

    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MCDetailView {
            var row = Int()
             // let indexPath =  self.tableView.indexPath(for: sender as! UITableViewCell)
            if  let indexPath = self.tableView.indexPathForSelectedRow {
                row = indexPath.row
            } else {
                let cell = sender as! UITableViewCell
                row = cell.tag
            }

            if segmentedControl == 0 {
                vc.detailMovie =  elements[row] as? MCMovie
            } else {
                vc.detailShow =  elements[row] as? MCShow
            }
        }
    }
}
